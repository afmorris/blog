---
title: Development Tools
---

I am a developer by trade. I am a power user on a PC, and I try to ensure that I
use the best tool or utility for the job at all times. This is my attempt at
documenting the tools that I use on a semi-regular basis, and ones that I would
consider as "must have" on any machine that I am working on.

# Desktop Applications

* [LINQPad](https://www.linqpad.net/) - My C# scratchpad and scripting tool. I
  use this tool to do anything from writing snippets of code that accomplish a
  singular goal to testing what a legacy application is actually doing in a
  method. I no longer write anything in PowerShell; I instead opt to write the
  script in LINQPad with the full power of C# behind it.
  * It can also connect to and query any database using LINQ (hence the name),
    but I admittedly use that much less than the scratchpad.
* [cmder](http://cmder.net/) - The console emulator that I choose. It's an
  extension of the already wonderful [Conemu](https://conemu.github.io/) with
  enhancements from [Clink](https://mridgers.github.io/clink/). It combines
  bash-style commands within a Windows environment without installing the full
  [Cygwin](http://www.cygwin.com/).
* [ReSharper Ultimate](https://www.jetbrains.com/dotnet/) - The ReSharper
  Ultimate license gives you access to the full suite of developer tools from
  JetBrains: [ReSharper](https://www.jetbrains.com/resharper),
  [dotTrace](https://www.jetbrains.com/profiler),
  [dotMemory](https://www.jetbrains.com/dotmemory),
  [dotCover](https://www.jetbrains.com/dotcover), and
  [dotPeek](https://www.jetbrains.com/decompiler).
  * I personally use ReSharper and dotPeek the most, as they are the most
    effective in day-to-day development.
* [Sublime Text](https://www.sublimetext.com/) - Text editor of choice. Combined
  with the amazing [Package Manager](https://packagecontrol.io/), Sublime is
  basically a full IDE, without all the bloat.
* [Beyond Compare](https://www.scootersoftware.com/) - Use this if you ever need
  to compare files or directories. Easily the best product on the market.
* [ShareX](https://getsharex.com/) - Screenshot tool of choice lately. I used to
  use [Greenshot](http://getgreenshot.org/), but ShareX's feature set trumps
  its. I've used the enterprise tool
  [Snagit](https://www.techsmith.com/screen-capture.html) previously, as well,
  but I don't feel like shelling out $50 for this tool.
* [Fiddler](http://www.telerik.com/fiddler) - Need to debug the network call
  your application is making? Use Fiddler.
* [SQL Server Management
  Studio](https://docs.microsoft.com/en-us/sql/ssms/sql-server-management-studio-ssms)
  * If you're connecting to a SQL Server database (or many of them), then just
  use the free Microsoft tool. It gives you everything you could ever want in a
  SQL IDE.
* [DBeaver](https://dbeaver.jkiss.org/) - If you're connecting to many non-SQL
  Server database instances (like I was at a previous job) you want to have a
  tool that will allow you to connect to all of them in the same window. DBeaver
  solves this problem, and it's free!
* [Dropbox](https://www.dropbox.com/) - Obvious addition. It's the best on the
  market for a reason.
* [Visual Studio](https://www.visualstudio.com/) - If you don't have a VS
  Subscription (which is pretty well priced, to be honest), then use Visual
  Studio Community Edition. It's so much better than it used to be.
* [1Password](https://1password.com/) - My password manager of choice. I've used
  [KeePass](http://keepass.info/) and [LastPass](https://lastpass.com/)
  previously, but 1Password won me over with its simplicity across all
  platforms. Also, the Windows application finally caught up to the Mac one in
  UX.
* [Semantic Merge](https://www.semanticmerge.com/) - When working on code diffs
  within git, it sometimes is difficult to visualize a diff due to
  reorganization of code. Along comes Semantic Merge, allowing for a 3-way merge
  tool that looks at the structure of the code, rather than the file as a
  string. Indispensable if you are doing lots of code merges.
* [Royal TS](https://www.royalapplications.com/ts/) - Pick your remote desktop
  manager of choice. I'll go with Royal TS, as it accomplishes everything I
  could want, including folders of connections, secure credential management,
  and easy ad-hoc connections.

# Chrome Extensions

I use Chrome as my daily driver browser, so naturally there are a number of
extensions that I use with it.

* [Postman](https://www.getpostman.com/) - Build and test your APIs, or any
  network call really. Lets you build out collections, has a robust
  export/import tool, and is just generally awesome.
* [uBlock Origin](https://github.com/gorhill/uBlock#installation) - Block all
  those unwanted ads. Required install in today's internet.
* [1Password](https://agilebits.com/onepassword/extensions) - A perfect
  companion to the desktop application of 1Password. Allows for easy random
  password generation and form filling.
* [Grammarly](https://app.grammarly.com/) - This handy free extension does
  spell-check and grammar-check against what you write as you write into
  textboxes. It's imperative for a blogger.

# Cloud Subscriptions

* [1Password for Families](https://1password.com/families/) - I pay an extra
  couple bucks a month for my entire family to use the same wonderful 1Password
  that I use personally. This lets us share vaults, set permissions, and do a
  host of other enterprise-grade security actions across all the passwords in
  our family.
* [Amazon Web Services](https://aws.amazon.com/) - The catch-all for the
  services I typically use. I'm typically on [S3](https://aws.amazon.com/s3/)
  and [CloudFront](https://aws.amazon.com/cloudfront/) for personal projects,
  but there are services for everyone out there.
* [DNSimple](https://dnsimple.com) - There are hundreds of domain name
  registrars you can work with. I choose to work with DNSimple because of its
  simplicity and being not-GoDaddy.
* [Pluralsight](https://www.pluralsight.com) - The best online learning platform
  for developers (and many other technologies and skills).
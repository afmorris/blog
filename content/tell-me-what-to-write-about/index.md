---
title: "Tell Me What To Write About"
---

I write about things as they come into my brain. I call this the "editorial
spark," and it doesn't always happen regularly.

If, by chance, you have an idea on something that you want my take on, please do
the following:

1. Go to the public Trello board where I keep the upcoming posts:
   [https://trello.com/b/7h62bb69](https://trello.com/b/7h62bb69)
1. Add a card to the **Upcoming** list of cards.
1. Tag the card with the bright pink **User Submitted** label.

I'll take a look at the ideas randomly, and I don't really promise to always
write about what you want, but I figure it's better to have ideas, even if they
don't come from me.

Thanks in advance!
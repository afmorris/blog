---
title: "Getting My AWS Cost to Nearly Nothing"
date: 2017-05-29
tags: ["software development", "amazon web services", "morrisphotos"]
aliases: ["/getting-my-aws-cost-to-nearly-nothing/"]
---

Some background: I run a couple of websites in [Amazon Web
Services](https://aws.amazon.com) (AWS). These include my photography website
project, [MorrisPhotos.com](https://morrisphotos.com). This post is a quick
overview of how I managed to save ~$55/month in AWS charges.

<!--more-->

# MorrisPhotos.com, Version 1

The first iteration of MorrisPhotos.com (available for view on GitHub
[here](https://github.com/afmorris/MorrisPhotos.com)) utilized the following
technologies:

* Front-End Technologies
  * [Twitter Bootstrap](http://getbootstrap.com/)
  * [jQuery](http://jquery.com/)
* Application Technologies
  * [ASP.NET](https://www.asp.net)
  * [ServiceStack](https://servicestack.net/)
* Data Technologies
  * [ServiceStack ORMLite](https://servicestack.net/ormlite)
  * [Microsoft SQL
    Server](https://www.microsoft.com/en-us/sql-server/sql-server-2016)
* Hosting Technologies
  * [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/)
  * [Amazon RDS](https://aws.amazon.com/rds/)
  * [Amazon CloudFront](https://aws.amazon.com/cloudfront/)
  * [DNSimple](https://dnsimple.com)

Lots of moving parts, but generally a very straightforward implementation of a
website. I would develop the new code locally for any features/bugs, then push
it up in a package to Elastic Beanstalk.

Similarly, I had a local SQL Server database that I would develop against, then
I would propagate any of the content and schema changes up to RDS.

## The Problem

At the end of the day, the hosted solution of Elastic Beanstalk+RDS meant that I
didn't have to pay attention to the uptime of my website, as Amazon took care of
that. However, this hands-off approach also meant that I was paying more, to the
tune of ~$55/month.

While I can handle that level of charge, I started thinking about **why** I had
it hosted in a platform. This introspection caused me to rethink how I was
running my website.

Additionally, the beauty of having a web application that has a live database
back-end is that changes are instantaneous. However, this precludes the **need
to change often**. MorrisPhotos.com is not a website that requires real-time
changes. In fact, I rarely would update it beyond a monthly refresh.

# Moving Forward

Given the two problems listed above, cost and "I don't need real-time database
updates", I set forth to change over the website to the following technologies:

* Front-End Technologies
  * [Twitter Bootstrap](http://getbootstrap.com/)
  * [jQuery](http://jquery.com/)
* Application Technologies
  * [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/)
* Data Technologies
  * [ServiceStack ORMLite](https://servicestack.net/ormlite)
  * [Microsoft SQL
    Server](https://www.microsoft.com/en-us/sql-server/sql-server-2016)
* Translation Technologies
  * [HTTrack Website Copier](http://www.httrack.com/)
* Hosting Technologies
  * [Amazon S3](https://aws.amazon.com/s3/)
  * [Amazon CloudFront](https://aws.amazon.com/cloudfront/)
  * [DNSimple](https://dnsimple.com)

## Local Development

The change in development strategies, given the different technologies above,
isn't much. I still am working in an ASP.NET web application (ASP.NET Core this
time around, as I wanted to work on some new tech) with a SQL Server back-end.

The difference is that I **do not** propagate the changes from the ASP.NET web
application and SQL Server back-end anywhere else; it is merely for local
development. Once I am done with the website updates, I merely run the (very
wonderful) HTTrack Website Copier program against my local website. This
generates a full static site of the web application, with all links intact.

As I already offload the photo hosting to Amazon CloudFront, this change in
generation creates a website that is very small (2.5MB in total). From here, it
becomes a simple exercise in uploading the generated HTML/CSS/JS to an S3
bucket, which has CloudFront in front of it.

To finish it off, I've updated the DNS records in DNSimple to now point to the
CloudFront distribution, rather than the Elastic Beanstalk ELB.

# Benefits

There are two main benefits here:

* Cost
* Speed

I don't have a finalized cost savings, but I expect my bill for MorrisPhotos.com
to be less than $1 now that it's merely hosted in S3 with the CloudFront
distribution in front of it. I'll update this page once I have a good monthly
number.

With regard to speed, this is measured in latency to the user, as there is no
longer any ASP.NET handler code executing, nor is there any database execution.
The only items loading are on the user side, and these are merely HTML, CSS, and
JS files.
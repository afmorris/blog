---
title: "Why Spotify Beats Google Music"
date: 2013-06-03
tags: ["tony chooses tech"]
aliases: ["/why-spotify-beats-google-music/"]
---

I wanted Google Music to be my destination for music. I really did.

Due to lack of a solid desktop app, a crappy Android app, and general
"meh"-ness, I am leaving Google Music and returning to Spotify.

<!--more-->

Some background: I've been a Spotify user since it became available in the
United States, and I have been generally happy. Previous to Spotify, I was a
Rhapsody user. Obviously, I am the type of user who wants the all-access type of
subscription. So, when I heard about Google Music's new offering, I thought
"This could be it!" Add in the potentially wonderful integration with my current
Android phone, and it all added up to me signing up for the free preview of
Google Music All-Access. With my ancient Sprint Samsung Epic 4G (SPH-D700), I
was ready to conquer the world of music with my friend Google.

While I tend to have my Chrome windows open at all times, requiring me to have
one open in order to play music on my desktop is a serious misstep. I know that
Google wants everyone to live in the internet browser world, but that's just not
my personal computing experience. I don't want to shut my music off when I close
all my browser tabs. Don't get me wrong, Google Music's web interface is
wonderful. It's responsive and pretty. But I just don't want to live in the web
in order to listen to music. Without a standalone desktop application (cross-OS,
preferably), I just won't use Google Music as much. +1 to Spotify, even if its
desktop app is a hog and a bit crazy.

I figured Google Music on Android would be a seamless experience. At first
glance, the interface on the app was everything I wanted it to be. I am in love
with the cards that Google has been using in their interfaces. The browsing was
intuitive and simple. It synced from the cloud almost immediately. However,
doing the simplest actions just crapped out for me. Skipping a song gave me a
solid 5+ seconds of silence before the next one played. This was for both
streaming music and music that I had downloaded onto the device. If I tried to
stop music, it would continue playing for ~10 seconds until it stopped. Of
course, it buffered the button presses, so if I got impatient and tried to hit
the stop button again, it would think I was hitting "play." In general, doing
the simplest of actions that a music application should be able to do (play,
stop, pause, next, previous) were just an abject failure and directly
contributed to me hating the application. However, I did appreciate that the
application stored downloaded song data onto the SD card, as opposed to the
Spotify app storing it in the device cache.

Thirdly, Google Music just **doesn't change anything** for me. I would be
willing to look past most of the pain points listed above if it did something
remarkable. If the music discovery piece changed my life and gave me something
wonderful to listen to, I would seriously consider staying with Google. The one
piece that makes me not want to leave is the power of the Google Music Radio.
Spotify has its own radio functionality, but it really seems like I am listening
to the same songs over and over again. Google Music Radio is seriously powerful,
and gives me music that I am sure to enjoy. I wish it would let me make a radio
station for an entire playlist, a la Spotify, but it's not terrible. This isn't
enough to make me stay with Google, however.

All in all, the items listed above all contribute to my unhappiness with Google
Music. I have officially uninstalled the Play Music app on my phone,
unsubscribed from the All-Access pass, and have begun reinstalling Spotify on
everything.
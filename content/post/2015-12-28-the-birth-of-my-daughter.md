---
title: "The Birth of my Daughter"
date: 2015-12-28
tags: ["personal", "family", "retro-diary"]
---

Back, by popular demand, is a running diary of major events in my life.
Previously, I wrote about [my wonderful experience with
PRK]({{< ref "2015-10-16-my-prk-experience.md" >}}) (and [its admittedly
not-so-wonderful
after-effects]({{< ref "2015-10-19-my-prk-experience-the-healing.md" >}})).

This time, I've decided to write about something that brings me great joy: the
birth of my daughter, Mattie.

<!--more-->

First, some background. On March 22nd (my real-life actual birthday), my wife
woke me up in the morning to tell me that our efforts were successful, and she
was pregnant! Great success!

Fast-forward 40ish weeks, and on December 5th at 6:33pm, my first child, my baby
girl, that wonderful angel you see beside this block of text, Mattie was born.
This retroactive diary is a way to tell you all the story of how this darling
little girl joined us.

---

# November 19th, 2015

Thanksgiving Day. Cindy, the kids, and I had plans to visit both my family in
Massillon and her family in Avon. Cindy is roughly 38 weeks pregnant at this
point, so we're on high alert to any signs of labor.

Thanksgiving lunch with my family goes swimmingly, and we are on our way up to
Avon. Upon reaching Cindy's family, Cindy is starting to feel a bit off, with
semi-regular (but slightly weak, according to Cindy) contractions every 5–10
minutes. Now, any self-respective father-to-be knows the 5–1–1 rule. If there
are contractions that are **5** minutes apart that last for **1** minute for
**1** hour, then it's time to call the doctor and head to the hospital. Needless
to say, I'm sitting at her relatives' place with bated breath, fully expecting a
mad dash to Medina Hospital.

After about an hour and a half of these weak contractions, I make the executive
decision to head back toward Seville (our home), just in case this is actually
labor, as Medina is much closer to our home than Avon is.

Long story short, once we head out of Avon, the false contractions stop
happening completely. They were most likely stress-induced, as Cindy's family is
full of high energy, Type-A personalities that were very excited at the prospect
of a new baby in the house. Please note that this isn't me complaining, and I
love her family dearly (as many of them are probably reading this right now!).

Fast forward a couple of weeks.

---

# December 2, 2015

Cindy and I attend her last regularly scheduled OB/GYN appointment, in which the
doctor informs Cindy that she's not as far along as he would hope, given that we
are at the 40 week mark. He schedules the date that she will be induced to
December 17, with hopes that our baby girl joins us before then. He informs her
to get an exercise ball and bounce on it, all in an attempt to get the uterus to
orient the correct direction.

Cindy spends the next three days bouncing on that ball.

---

# December 4, 2015

Late in the evening, Cindy is bouncing on the exercise ball and mentions to me
that it "feels different." I disregard this.

---

# December 5, 2015

## 2:30am

Cindy wakes up with gas pains. I disregard this.

## 7:00am

Cindy's gas pains are continuing (rather, they've continued this entire time,
but I've slept through most of them). I start to regard this a little closer,
especially when she shows me the regularity of these gas pains. Every 5–7
minutes, and they seem be quite debilitating.

## 8:00am

I am no longer disregarding the gas pains, as they are coming every 4–5 minutes.
I call her doctor, who tells me that we should start getting ready to go to the
hospital. I am nervous beyond all description.

## 8:45am

Nearly to Medina Hospital. We are waiting to turn left from the I-71N off-ramp
onto SR-18. The light won't change. I swear it's been at least 5 minutes, and
the other lights have cycled completely. Why won't the freaking light change. I
turn right instead (away from Medina Hospital), and I do a (very legal) U-turn.
I contemplate how long I would have been sitting there, as the cars that were
behind me on the off-ramp are still sitting there.

## 9:00am

We arrive at Medina Hospital. I drop Cindy off at the door while I head to the
parking lot.

## 9:05am

I walk into Medina Hospital to a Santa Claus event. I am unable to find my wife.
I contemplate what could have happened in the five minutes that I was gone, then
I just decide that she didn't want to stand around the seriously loud piano and
children. I head up to the Family Birthing Center.

## 9:10am

I arrive at the Family Birthing Center to a nurse saying "She said you'd be
right up. Head on in!"

---

Quick segue here. I'd like to just take a quick moment to write about how
**amazing** the nurses, doctors, and support staff are at Medina Hospital,
especially the Family Birthing Center. Literally every person that we
encountered were just the best kind of people, and it made our experience there
stress-free and perfect.

---

## 9:15am

Cindy is right around 3cm dilated. This is really happening. I cannot wait to
meet this little girl.

## 11:30am

Cindy is going through some pretty rough labor pains during contractions. She is
measuring about 6cm. The nurse tells Cindy she can get an epidural at any point.

Cindy gets an epidural.

## 12:00pm — 5:00pm

Not much to report during these times. Cindy is numb from the waist down now
that she's got a freaking needle in her spinal column. I am doing generally
nothing, except internally imagining what this little girl is going to be like,
informing the many family members of the current situation, giving play-by-play
information to my brother and Cindy's mother, and thinking about how I should
have eaten breakfast. Cindy and I are in good spirits, and we are excited for
the next steps.

## 5:00pm

Tommie, Joey, and Carol stop by! We had originally told them that we'd let them
know when to come (my original plan was to tell them as we were going into
labor, which would then have them arrive after labor, mainly so they wouldn't
have to wait for so long). However, Tommie was having none of it, and forced her
grandmother to bring them. It was really great to have the family there. I send
Carol off to get me Wendy's, as the end of our little journey here is coming
quickly.

## 5:30pm

Cindy's OB comes in, takes a quick look, and informs Cindy that we're going to
start delivery. I guess I don't get Wendy's just yet.

## 5:30pm — 6:32pm

I am given the job of holding my wife's leg like a shotgun while she pushes.
Literally, I felt like I was cocking a shotgun and the shell that would come out
is my daughter. Additionally, the doctor tells me to count 10 seconds every time
a contractions rolls through, in sets of 3. He later tells me I count too
slowly.

Not much else to report here, to be honest. Witnessing the birth of your child
firsthand is something that I would recommend to all fathers-to-be. I cannot
give the experience justice with the written word, but let me just say that it
is really just the best thing I've ever experienced in my life.

I continue to count too slowly. Cindy does not appreciate it.

## 6:33pm

Mattie Nicole joins us in this world with a scream and a whole bunch of poop.
Seriously, poop everywhere. I cut her umbilical cord. I watch as my beautiful
little daughter, my darling angel is cleaned up and laid on my wife's chest.
They say that skin-to-skin contact for a newborn is one of the most important
experiences it can have. I agree, as I watch my daughter lay with my wife. I
realize that there is nothing in this world that will ever be the same to me. I
am happy.

## The rest of the evening, the next day, and the next few weeks

Tommie, Joey, Carol, David, and Lindsey come to visit my baby girl, and their
new sister/granddaughter/niece after a number of hours. Later, Christine, Greg,
Landyn, Preston, and Adrianna come to visit. A lot of pictures are taken. We
eventually bid adieu to the family, as I sleep in a pullout chair, Cindy sleeps
in her hospital bed, and Mattie sleeps comfortably in the crib.

Mattie slept well that first night. So did Mom and Dad, knowing that she was in
our lives.

The next day, Cindy's OB stopped by and informed us that we could go home that
night if we wanted to. We enthusiastically said yes, and we eventually made it
home around 9pm. That first night home was a welcome relief, as we got to start
our lives with the new family member.

Throughout the next few weeks (it's been over 3 weeks now), we've come to learn
more and more about our little Mattie. She is a very quiet baby, one who doesn't
fuss unless she is hungry. She sleeps for a solid number of hours through the
night. I, however, am still tired every morning. I'm also a bit of a complainer,
so take that with a grain of salt.

My life has changed so much in the past couple of years. Between meeting Cindy,
moving in with her, buying a house together, joining our lives in marriage, and
now creating this wonderful little bundle of joy, I will never stop being
surprised at how amazing everything has worked out for me.

I am happy.
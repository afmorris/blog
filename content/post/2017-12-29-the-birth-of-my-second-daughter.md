---
title: "The Birth of my Second Daughter"
date: 2017-12-29
tags: ["personal", "family", "retro-diary"]
aliases: ["/the-birth-of-my-second-daughter"]
---

I haven't written a retroactive diary in awhile (ever since [Mattie was
born]({{< ref "2015-12-28-the-birth-of-my-daughter.md" >}})), so I figured I'd
resurrect the process for the birth of my second daughter, Macie.

<!--more-->

Previous iterations of the retro diary include:

* [Wonderful Experience with PRK]({{< ref "2015-10-16-my-prk-experience.md" >}})
* [Not-so-Wonderful Healing Experience from PRK]({{< ref
  "2015-10-19-my-prk-experience-the-healing.md" >}})
* [Birth of Mattie]({{< ref "2015-12-28-the-birth-of-my-daughter.md" >}})

Some background on this pregnancy. In mid-January, my wife tells me that she's
peed on a couple of highly-technological items, and they've come back saying
that there's a solid chance that we've successfully created a baby. Great
success!

Fast forward 40ish weeks, and on October 6th, 2017 at 5:37am, our second little
bundle of joy, Macie Lynn Morris, was brought into this world. This retroactive
diary is to tell you the story of her birth. Spoiler alert: it's much much
shorter than her sister's story, which you'll find out later.

---

# Preamble

Just a quick note here about the upcoming birth of Macie. If I'm very honest
with myself, I was really anxious about what life would be like with another
baby in it.

I am so fulfilled with Mattie in our lives, that it makes it very hard to
imagine would life would be like with **more**. Obviously I was excited to have
another little mini-me in my life, but I had no idea how I was going to juggle
the time and emotions required for another version.

These concerns completely evaporated once I saw Macie, and I can now finally
understand The Grinch when his heart grew three sizes in one day.

---

# September 28, 2017

39ish weeks along in the pregnancy. Cindy's final appointment with her OB.
Things are progressing about as quickly as they did with Mattie (read: they
weren't). Induction plans were set for the third week of October if need be.
Cindy gets her membranes stripped (which legit sounds terrible), and we're off
for the rest of the day.

Later that evening, Cindy starts to have some serious Braxton Hicks
contractions, to the point that we thought Macie was going to be born that
evening.

Nonetheless, nothing happened. Fast forward a week.

---

# October 5, 2017

## 10:00pm

Cindy and I head to bed around our normal time. She's feeling super pregnant,
but nothing out of the ordinary. We sleep (really, I sleep, because she's
seriously pregnant and probably wasn't sleeping too hot).

---

# October 6, 2017

## 1:30am

Mattie wakes up crying. As Cindy is already on PTO for the week (took it before
maternity leave), she bites the bullet and soothes her back to sleep downstairs.

## 2:00am

Cindy calls me three times, and my phone is on silent. Whoops.

## 2:15am

Cindy yells up at me, telling me "It's time go to the hospital." I wake up in
approximately 0.3 seconds, get dressed and everything packed by 3:00am.

## 3:00am

We are on the road, heading toward Akron General (because Medina Hospital
decided to close their birthing center). This is not a fun drive, littered with
potholes on I-76E. We got lucky that there wasn't any traffic.

## 3:45am

I drop Cindy off at the ER entrance, as it's the middle of the night, and go
park the car.

## 4:00am

I get up to exam room that Cindy is in, and she's having contractions pretty
regularly by this time. It's baby day!

## 4:15am

We get moved to the birthing room down the hall. Luckily we got a bigger room,
so I'm not feeling as cramped as possible. Still missing Medina Hospital though.

## 4:45am

Cindy is dealing with some serious labor pains. The doctor orders an epidural,
which should be coming in a few minutes.

The nurses are awesome, and help Cindy out in every way possible.

## 5:00am

The epidural arrives! It goes in after about 10 minutes, and Cindy starts to
feel relief (eventually).

## 5:20am

The doctor on staff comes by, and he decides to break Cindy's water. Meanwhile,
Cindy has more anesthetic pushed into her epidural.

I figure we've got a long wait ahead now that she's got the anesthetic in. It
took Mattie a solid 6ish hours to come out after.

## 5:23am

Cindy is in a serious amount of pain, even through the epidural, so I buzz the
nurse. She comes in, takes a look, calls the doctor in, and he tells us that
Macie is trying to join the world now! Guess we're not waiting around!

## 5:37am

After about 7 minutes of pushing, Macie comes out! Not as much poop as Mattie,
and with a head full of dark hair! Guess we only have one redhead in the family
so far.

I cut her umbilical cord (those things are seriously tough), and I watch as she
gets cleaned up and put on my wife's chest for the ever-important chest-to-chest
contact.

As before with Mattie, I am happy.

## The rest of the day, the next evening, and the next few months

Macie is healthy. Cindy is healthy. Life is good.

We spend a total of 36 hours in the hospital. Cindy's OB swings by, as she's on
staff that Saturday, and tells Cindy that we can go home that evening.

Cindy and I bring our little darling Macie home that evening. She was super
quiet on the entire drive home, and little did we know that would set the tone
for the next few months with her.

Mattie was a great baby: always slept and ate well, didn't cry much (except for
about a 3 week period), and was generally awesome. We didn't expect our next
child to be anything wonderful, because of mainly karma.

Macie has been everything Mattie was great at, and more. She sleeps great. She
eats better than Mattie ever did. She doesn't have any milk digestion issues
that Mattie had for a few weeks. All in all, our darling girl is a quiet,
introspective young baby, and she's made our lives the better for it.

Speaking of Mattie: she absolutely adores her baby sister. She is always ready
to give her hugs and kisses, and frequently "helps" Cindy around the house when
they're all home together.

We've had the good fortune to bring both of our little girls to many family
outings recently, between OSU/Michigan, Thanksgiving, birthdays, and Christmas,
and everyone is in love with the both of them. We're very lucky to have the
family and friends that we do, and I'm not quite certain what I did to deserve
everyone.

As before, I am so happy.
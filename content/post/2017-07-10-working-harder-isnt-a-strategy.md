---
title: "Working Harder Isn't a Strategy"
date: 2017-07-10
tags: ["software management", "rants"]
aliases: ["/working-harder-isnt-a-strategy/"]
---

As a technology professional on the development side of the house, I regularly
have to provide estimates for when my work will be done. This work includes
writing code, packaging up a deliverable, writing documentation, or anything in
between. Following the estimate delivery, there is a level of **trust** that
exists between the technology delivery team and the project/account management
team in that the estimate 1) will not be inflated by the technology team, and 2)
will not be altered by the project/account management team.

<!--more-->

# The Situation

> Going to need everyone to buckle down!

Just over two weeks ago, we were informed that the next release (let's just call
it vNext) needs to go out "quickly." This happens from time to time, mostly due
to unforeseen complications with our 100s of clients that are expecting
features. Not that big of a deal.

Except that "quickly" is actually within **days**.

Except that not all the functionality is known/completed.

Except that the estimated testing timeline is ~3 weeks.

Except that this release contains **another** release that went to a different
environment previously.

You get the picture. I could go on and on for this one.

# The Rage That Consumes Me

> It's a bit of a push but let's do our best to make this window.  
> -- _Project Management_

Everyone look at that quote. Absorb it into your consciousness, because it's
**not a solution to a bad situation**. Let me enumerate why.

* This is being said by the **project management** team. As much as I respect
  PMs, they do not have skin in the game. They do not control delivery of any of
  the technical aspects (hence not being in the technical delivery teams). The
  PM team saying that "we" need to do "our" best makes no sense here, as it
  doesn't include that team.
* It assumes that everything up to this point hasn't been the absolute peak
  output for the team. Said another way, it assumes that people aren't working
  as hard as they could be. **This is insane**, and it's an insult to the teams
  that have been busting their butts.
* It **breaks the trust** that was carefully crafted between the technical
  delivery teams and the project/account management teams. The technical teams
  (the QA team in this case) gave an estimate of ~3 weeks for functionally
  validating the release (137 issues in the release will do that). By dismissing
  the estimate, the project/account management teams are effectively giving the
  technical delivery teams a good reason to provide higher estimates on tasks.

If the project/leadership/account management teams cannot be trusted to accept
the timelines that are delivered to them from the technical delivery teams, then
it is up to the technical delivery teams to inflate every estimate that gets
delivered to protect themselves.

# What Should Really Happen

I work with a technology with a number of high-profile clients, including some
who are also on our Board of Directors. I really do understand that sometimes a
bad situation happens because of a personal relationship.

In that (hopefully very rare) occurrence, it's time to call it out upfront and
create a plan. If the client is expecting Feature X so they can do Activity Y,
then that is all we're delivering on the crappy timeline.

This requires a hard introspection on the items that are queued for delivery,
but it's the best way to minimize the risk from an already-bad situation. It
will also likely cause hard conversations with other clients about why "their"
functionality isn't making it out for a release. That's why they call it "work,"
I suppose.

At the end of the day, there are three items that can be controlled in all
projects: Scope, Budget, and Timeline. However, given a project, you can only
control two--the third is a dependent variable.

This means that you can't control scope (the number of tickets in a release),
budget (the number of people working on the release), **and** the timeline of
the release at the same time. If you shrink the timeline, but keep the budget
the same, the scope **must** be shrunk as well. Asking your team members to
compensate for the misunderstanding of this core truth by working harder is
short-sighted and takes advantage of people's good intentions.

# Wrap It Up

This terrible, no-good situation happens more than it should in technology
companies. It stems from a number of places, but it most commonly comes from the
world in which **the technology is not the product**. Said another way,
client-service companies, regardless of what they build or do, will always have
this problem because the product is the service to the clients.
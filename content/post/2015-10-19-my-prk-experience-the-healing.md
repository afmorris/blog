---
title: "My PRK Experience: The Healing"
date: 2015-10-19
tags: ["personal", "health", "retro-diary"]
aliases: ["/my-prk-experience-the-healing/"]
---

My wife decided that it would be slightly irresponsible if I only posted about
the wonders of modern medicine without discussing the (potential) drawbacks.
While my eye surgery was (so far) a resounding success, there naturally were
some post-operative issues that I'll go into detail here.

<!--more-->

# Background

As my last post detailed, I had
[PRK](http://www.allaboutvision.com/visionsurgery/prk.htm) (similar to LASIK,
but different in a very specific way — there is no corneal flap) on Friday,
October 16, 2015. A post detailing the day of surgery and its immediate
after-effects can be seen here. Long story short, everything went great. I was
in and out of the surgical room in about 8 minutes, and I left [Lasik Vision
Centers of Cleveland](http://www.lvclasik.com/) about 90 minutes after I
arrived. While the end of that previous post detailed happiness and joy, I was
about to come to a rude awakening. I'll attempt to explain in a live-blog of
sorts.

# Day 1: Saturday

2:30AM: Stabbed in the right eye ball. That was how I woke up my first night
post-op. Later come to find out that it was most likely the bandage contact lens
getting stuck on my eye lid. Super. Fell back asleep eventually.

6:00AM: Woke up naturally to the constant ache of my left eye feeling like it
had sandpaper on it.

6:15AM: Begin the first round of medicated eye drops. To recap the drugs I get
to take: 1 drop 4x daily of a steroid that makes me see milky white, 1 drop 4x
daily of an antibiotic that has the absolute worst taste, 1 drop 2x daily of a
pain reliever that doesn't appear to do much at all, and 1 pill 3x daily to act
as a nerve blocker (to help with pain).

7:00AM: Attempt to go downstairs. Immediately determine that sunlight is the
worst thing ever, and I hide upstairs.

8:30AM: I wake up late to my brother picking me up, as I have my 1-day post-op
appointment with my optometrist (the wonderful Dr. Beiling at [Medina Vision and
Laser Centre](http://medinavisionandlaser.com/)).

8:40AM — 9:00AM: I hide my eyes from oh-my-goodness-the-brightest-sun-ever
during the drive to the optometrist. I consider life as a vampire.

9:00AM: In a quick meeting, Dr. Beiling says everything is a-okay and to stay
out of the light.

10:00AM: I hang out with my brother and his girlfriend for a little while in the
living room. By "hang out," of course, I mean that we sat in the living room
while I had sunglasses on and a hood over my eyes. Eventually, they head out,
and I head back to my cave (the bedroom).

11:00AM — 6:00PM: I intermittently leave the cave, be it to put drops in, get a
small bite of food, or drink some water. I inevitably end up running back
upstairs to my hovel.

---

I would like to take a break to properly explain what's going on in my cave. Our
bedroom is pretty dark naturally, and the gray curtains certainly help that.
However, to a person who is very light sensitive, it's like living on the
surface of the Sun. To combat this, I closed every door, took Cindy's large
maternity pillow and covered my face with it, then propped up another series of
pillows so I essentially was in a fort. A wonderful pillow fort of darkness.

---

6:30PM: My uncle, Kevin, comes over. We are watching the Ohio State — Penn
State game tonight at 8:00PM. I am genuinely concerned that I won't be able to
watch my Buckeyes win. I am determined to find a way to watch the game.

7:30PM: Kevin and I head out to Romeo's in Lodi to pick up the pizza that I
ordered ahead of time. I get a phone call from Cindy informing me that The
School Up North managed to lose from ahead against Michigan State. Hilarity
ensues. This didn't really have anything to do with my eyes, but I felt like I
had to include it.

7:59PM: With Bryan over, as well, I determine that I can handle the horrible
dryness and pain by taking a cold rag and holding it against my eyes during the
breaks between plays. I am ready to watch the game!

---

Another side note. To let me be **more** able to watch the game, we ended up
turning the brightness on the TV in the living room all the way down, and
essentially turned off the backlight. It helped.

---

8:00PM — 12:00AM: No idea what time the game ended, but my process of putting
the cold, wet rag on my eyes between plays was a resounding success!
Additionally, I was completely bullheaded about the sandpaper eyes that I was
experiencing, so maybe I'm just telling myself it helped. By the end of the
game, I was watching the backside of the rag more than the game. I kicked Bryan
out, Kevin went to bed, and I went to hide in my cave. Go Bucks!

# Day 2: Sunday

6:30AM: Sprint to the bathroom to put in the eye drops, as my sandpaper eyes
were **absolutely killing me** at this point. Maybe I overdid it last night.

7:00AM — Most of the rest of the day: I hid in my cave. Don't get me wrong — I
have a high pain tolerance. This was absolutely **miserable**. The only respite
I could find was hiding in the darkness. If I went out into the real world
(read: downstairs) I would be aching within minutes. I tried to grin and bear it
some of the evening, but I legitimately couldn't leave the bedroom until the sun
went down.

Post-dusk: I was able to come downstairs. Cindy had made a wonderful meal. While
my family ate it sitting in the living room and watching TV, I ate it sitting
alone in the dark dining room, as the light from the TV was too much. After
dinner, I laid on the couch, facing away from the television. I was absolutely
miserable, and I was seriously questioning why I did this. Then I went to bed.

# Day 3: Monday

6:00AM: More sprinting to drops. The bandage contact lenses are really starting
to bother me, as I could definitely feel them while I was sleeping. I have
another post-op appointment to replace the lenses, so maybe that will help.

7:00AM — 8:00AM: I hide behind sunglasses and a hood over my eyes as Cindy
drives us to drop the kids off at school and then to my appointment. Starting to
look like the Unabomber.

8:00AM: Dr. Beiling removes the 3-day-old bandage contact lenses. Everything is
immediately better. She looks at both eyes and tells me that my epithelial layer
is 100% healed in both eyes (hurray!), but she's going to put a new set of
bandage contact lenses on (boo!) until Wednesday, as they're still tender. I
notice an immediate change with my light sensitivity, as I can actually open my
eyes when light is present. I notice that my bedroom is not, and has never been,
the surface of the Sun.

# Recap

All in all, my vision is right around 20/40 at the time of this writing. It
comes into clarity at nearly 20/20 and then fades back to 20/40 as the day
progresses. This is just my eye healing itself, and it's expected.

Additionally, the dryness and discomfort that I was experiencing in my eyes was
absolutely **no joke**. This was a horrible, miserable healing experience. While
it doesn't make the end result any worse, and I would absolutely recommend this
type of eye surgery to anyone, it is absolutely something to think about when
you are planning.

It's about 5:00PM on Monday, and I've been working in front of a computer since
about 11:30AM today. I had to put in lubricant drops about a dozen or more times
today ([Refresh Optive](http://www.refreshbrand.com/), ftw), but that was just
as my eyes were drying out. As the day has gone along, I've had to put the drops
in less and less frequently, which makes me very confident for the future. I've
got another post-op appointment on Wednesday, in which they will remove this set
of bandage contact lenses, and then it's all downhill from there.

As a person with terrible vision his entire life, having this surgery was the
best decision I could have made.
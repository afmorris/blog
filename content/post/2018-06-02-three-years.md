---
title: "Three Years"
date: 2018-06-02
tags: ["family", "personal"]
aliases: ["/three-years/"]
---

We have now completed Year 3 since my mother, Kathy, passed away after an
extended battle with lung cancer. Since [last
year]({{< ref "2017-06-01-two-years.md">}}), I began posting what she's missed
in the past year.

<!--more-->

# Previous Years

* [Two Years]({{< ref "2017-06-01-two-years.md">}})
* [One Year]({{< ref "2016-06-01-one-year.md">}})

# June 2017

* Cindy, Tommie, Joey, Mattie, Carol, and I travel to Northern Michigan for a
  week-long vacation in a house with no air conditioning in the middle of
  summer. It was so uch fun to get away, hike through some amazing trails, and
  just experience nature and openness.
* Father's Day 2017. It hasn't really dawned on me that Father's Day is for me,
  but I'm enjoying it.
* Mattie got a lot of outside time in her kiddie pool. She absolutely loves the
  water, just like David and I did when we were younger.
* Another family vacation, this time with Christine, Greg, and their kids at a
  log cabin in Hocking Hills. Just an absolutely wonderful long weekend spending
  it with family.

# July 2017

* Mattie is growing up so fast. She's eating with utensils, and she loves
  macaroni and cheese. This still hasn't changed.
* David's bachelor party! I got to plan it, being his best man. We went to a bar
  that has arcades for free, then we watched an Indians game afterward! It was a
  lot of fun, and David had a great time.
* We put a swing up for Mattie on one of the trees outside the house. She loves
  everything about swinging.

# August 2017

* You turned 54 this year! Missing you just as much as the day before.
* Fun trip with the family to Kalahari, which is just the greatest place on
  Earth.
* David and Lindsey got married! This was a great day, seriously. I gave a toast
  at the reception, and we all thought of you collectively. Missed you so much
  here, Mom.
* David is getting older at 33 now. Basically on the downslope of his life.
* Cross Country season starts for both Tommie and Joey. This is going to be a
  fun year with Tommie being a junior and Joey being a sophomore.
* I changed jobs for the third time in my life to be closer to home. More on
  this later.

# September 2017

* Cindy and I's three year anniversary! It's been three of the best years of my
  life, and it's just getting better.
* I discovered cookie butter thanks to a good friend. It's like peanut butter,
  but better. You'd love that I love it.
* I built a podium for the cross country team's home invitational, with the help
  of Cindy and Tommie and many hours of work. It was hard work, but I'm glad I
  was able to give back to our home school.

# October 2017

* Macie Lynn Morris was born on October 6 at 5:37am. This little girl is
  perfect, and you better believe she'll know her grandma when she gets older.
* Mattie keeps growing up. She's singing the ABC's and loves to dance, and she
  loves her little sister.
* Halloween with all the kids was so much fun. Mattie was a monkey, and Macie
  was a banana. It was adorable, and I loved it.

# November 2017

* Tommie turns 17! She is becoming such a wonderful young woman, and you two
  would have gotten along so well.
* Ohio State beat Michigan (again). This is pretty regular now.
* We took some family pictures (all six of us). I would have loved to send you
  one of our Christmas cards so you could fawn over your babies.
* We decorated our Christmas tree! This is one of the things we love to do in
  the winter as a family.

# December 2017

* Mattie turns 2! We didn't have as large of a party--mainly just family, but it
  was still something we loved.
* Christmas Eve at Grandma and Grandpa's house again. As before, this is the
  hardest day of the year for me, but it's great to see Mattie and Macie get
  surrounded by the love of my family.
* I got the flu. I've never had the flu (from what I can remember). I don't
  think I've ever been closer to death, and Cindy made sure the house was
  sterile, as we had a newborn in the house. She's a champ.

# January 2018

* Joey turns 16! He's going to be driving soon, so we are warning people to stay
  off the roads!
* Macie just keeps growing. I thought she was supposed to stop growing at some
  point, but I guess that's not in the cards.
* Mattie is turning into a fun toddler. She loves to dance, hold my hand, and
  laugh. She also loves her little sister so much.
* We went to Disney on Ice, which was an absolutely adventure. Mattie loved it
  with all the singing and dancing, and we are excitedly prepping her for Disney
  this summer.

# February 2018

* Nothing too exciting in February, except both Macie and Mattie getting another
  month older. Macie is turning out to be a very chill baby who rarely gets
  fussy, and Mattie is turning out to be a sassy little toddler. Sounds about
  right.

# March 2018

* I turned 32. I'm blessed to be surrounded by friends and family every day, and
  I am continually reminded of you on important days like this.
* I switched jobs (again). You'd be worried, but okay with it, because of the
  flexibility of my new job.
* I started my second year as a Track and Field Coach at Cloverleaf, coaching
  the throwing events. I love every minute I spend with those kids, and you'd
  love to come watch me coach.

# April 2018

* Macie is developing quite a personality. She loves to giggle at her silly dad.
* Tommie is having a very special track season as one of the top runners in the
  state in the 800. You'd be so proud of her.
* Joey is really developing into a good 3200 runner. He's going to surprise some
  people by the time he's done with high school.

# May 2018

* Most of this month is dedicated to track season, between coaching and watching
  Tommie and Joey run. Cindy is a superhero and watches the girls so I can have
  this opportunity to do what I love.
* Tommie goes to Prom! She looks absolutely beautiful, and she is getting way
  too grown up. Her date was an absolute gentleman and a great kid.

--------

I guarantee that I missed some items in this list, but that's all I can type out
tonight. I miss you more and more as the years go by, Mom. Love you.

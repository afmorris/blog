---
title: "Five Years"
date: 2020-06-13T19:42:27-04:00
tags: ["family", "personal"]
---

One thousand eight hundred thirty-nine days since my mother, Kathy, passed away after a battle against lung cancer. Since 2017, I began posting what she's missed in the past year. This helps me both connect with her and recap the last twelve months.

<!--more-->

# Previous Years

* [Four Years]({{< ref "2019-06-01-four-years.md">}})
* [Three Years]({{< ref "2018-06-02-three-years.md">}})
* [Two Years]({{< ref "2017-06-01-two-years.md">}})
* [One Year]({{< ref "2016-06-01-one-year.md">}})

# June 2019

* I got the opportunity to coach a state champion shot putter. Brooklynn's an amazing athlete and an even better person.
* Stopped by the Medina County Fairgrounds for Relay for Life 2019 to support Judy and remember you. Judy's family always does such a wonderful job making all of us feel like one of the family.

# July 2019

* Bryan got married! Cindy, Micah, and I all went down to Berlin to watch Bryan get married to Brittany. We hung out with Austin, Terri, and James all night. It was an absolutely wonderful wedding and reception in a beautiful location.
* Possibly the last Lodi Sweet Corn 5K held during the Lodi Sweet Corn Fest. It's always great to see the community come out and support the cross country teams.
* 4th of July at Judy's house! This has turned into an annual event for the family, and everyone just loves it.
* I got promoted to **Enterprise Architect** at Hyland. You wouldn't have known what this means (and, let's be honest, most of the world doesn't), but this is a pretty big deal in my career. You would've been so excited for me.
* The whole family went on vacation to the Ocean City, Maryland area. Totally different vibe than our typical Outer Banks visit, but it was really great!

# August 2019

* David's getting older. One day he'll have gray hairs, but today is not that day.
* Your 56th birthday. I miss you so much.
* Joey's senior year in high school and Tommie's freshman year in college all get started! They're growing up so fast. I distinctly remember asking them to not grow up, but apparently they didn't want to listen.

# September 2019

* Cross Country season for Joey continues. This team is doing really well. They might be something special by the end of the year...
* Cindy and I's fifth anniversary (the "wooden" anniversary). We both got each other pretty unique gifts. It's been an amazing, and fast, five years.
* The little ones just. keep. growing.

# October 2019

* Macie turns two! She is _such_ a middle child sometimes, so it's really nice to have a whole day to celebrate just her. 
* Went down to Dover to catch a football game and have a mini-15 year reunion. It was nice to sit in Crater Stadium and watch the Tornadoes again. Grandma and Grandpa watched the kids while Cindy and I were able to get out.
* Yearly family photos! The photographer we use has taken photos of us for the past few years now, and every time they're absolutely wonderful.
* Joey qualified for the Cross Country State Championships with the rest of the boys cross country team! I can't begin to explain how over-the-moon excited everyone was for this.

# November 2019

* Tommie's last birthday as a teenager! It's been amazing watching her grow up into the young woman that she is today.
* Joey competes at the Cross Country State Championships! Cindy and I rented an RV and hosted a tailgate for all the Cloverleaf friends and family. It was completely ridiculous and awesome. I loved watching Joey run at this level of competition.
* Ohio State beat Michigan. Again. It was great having all our friends come over to celebrate again.
* Disney+ is released. Imagine all the Disney movies ever created able to be streamed. Let's just say the little ones love this (and maybe so do I).
* Thanksgiving! We did the double Thanksgiving again--lunch at Dick and Becky's, then dinner at Cindy's family. It's a long day, but it's great to see everyone on the same day.

# December 2019

* Mattie turns four! I **cannot believe** that four years have gone by already. It seems like she went from little girl who had to be carried for an hour at night so she could sleep... to this fiery redhead that is the bossiest little girl imaginable. I love her.
* Cindy's birthday! I love this woman.
* Christmas 2019! As usual, we went down to Dover for our little family Christmas at Grandma and Grandpa's house. The little ones are getting old enough to get really excited about this trip, and the big ones really appreciate being around more family. Cindy and I are blessed to have so much close family. I miss you more than I can explain on Christmas, as always.

# January 2020

* I jumped in a frozen lake for charity. It was exhilarating and insane, but it was great to jump in alongside Joey's cross country teammates and my coaching friends!
* Joey turns 18! He's officially an adult! I'm so proud of the man he's become throughout these years.
* Micah _really_ wants to walk. I _really_ want him to slow the hell down with growing up.
* Cindy, the little ones, and I go to visit the Cleveland Museum of Natural History. I've never been there before, and it was a lot of fun! Mattie and Macie loved the dinosaurs (and so did I)!

# February 2020

* My first major injury as an adult: a torn bicep on my right arm. I decided to try to help lift something super heavy, and it just exploded. Super great times had by all. Surgery coming soon...
* Track season starts! I'm super excited to have another season to coach some of the best kids I've ever known.

# March 2020

* Micah is a year old already! This is happening so quickly. While I'm looking forward to the days that I can go out with him to throw the baseball, I'm really trying to take this slowly. It's hard for me to explain how I _know_ he's growing up, but I'm just not seeing it regularly. That is, until all at once, I'm like "wait, when did he start throwing away his own dirty diapers?"
* I turn the big 3-4. I feel like 2019 was the slowest year of my life--I was 33 forever. 33 was great, and I'm sure that 34 will be too.
* Covid-19 hits the United States really freaking hard. More than 100,000 people will have died by the time I write this. It's absolutely terrible. Luckily, Ohio did smart things and had an actual doctor lead the response to the virus, so the number of people who got sick and/or died is significantly lower than it could have been.
* All spring sports are cancelled through the year. No track season. This is weird.
* All schools are remote only. This is weirder.
* Hyland informs all its employees to begin working from home. This is going to be a long few months.

# April 2020

* Along with the statewide shutdown, Ohio has begun allowing takeout alcohol orders from restaurants. Cindy and I take advantage of this a few times with our local Mexican restaurant and their amazing margaritas!
* Micah can do pull-ups on his high chair. He's awesome.
* We got some trees taken down from the yard before they fell on the house. As you very well knew, I hate trees (future property damage), so this is totally fine. We'll be getting more taken down soon.

# May 2020

* A few family bike rides with all the kids and Carol! It's really great to be able to do all of this together.
* Joey graduated from Cloverleaf High School! Even with all the shutdowns, Cloverleaf did a great job with making sure each and every senior had his/her opportunity to shine. Each of them got to walk across the stage, we got to get pictures, and it was, in general, very well done.
* The biggest bright spot in the horrible that is Covid-19 is how much time we've gotten to spend with family (at least here in Seville). It's been great to spend this time together, even if we're just sitting on the porch.

# June 2020 (so far)

* The world is on fire. Not literally speaking, but figuratively. A black man was murdered by a police officer on video, and it **finally** clued the country in to the systemic racism that still exists to this day. My beliefs are pretty clear on this fact, especially regarding the militarization of the police, and I would have loved to hear your perspective on this.
* Dover High School's old wing is being demolished (finally). I have a lot of memories in that wing of the building, mainly centered around me being on crutches my sophomore year and tumbling both up _and_ down the half-stairs.

--------

I miss you, mom. I find myself thinking about you all the time. I still have a note from you that says "Love you all" on our fridge here at home. It's never going anywhere.

Mattie talks about you. She mentions about her "three grandmas" pretty regularly (Cindy's mom, Grandma Cookie, and you). Your pictures are up in the house, and every one of my children will always know who you are and what you meant to this family. Quite simply, I'm not half the person I am today without you raising me. Without you now, I'm a little broken, but every day I think about how you would hold our family together through both the good times and the bad. I strive every day to be more like the person you were. I'll get there one day, but I'm certainly not there yet.

Until next year. Love you.
---
title: "My Core Values"
date: 2018-04-18
tags: ["personal", "software management"]
aliases: ["/my-core-values/"]
---

I've been doing professional software development for nearly 10 years now.
Throughout that time, I've worked at a number of organizations, I've been
managed by a number of different people, and I've attempted to absorb as much
knowledge (both technical and non-technical) as possible. After that long of a
time, I've come to realize a bit about what makes me who I am, and what drives
me as a person and developer.

<!--more-->

I like to call these **My Core Values**. These
core values are as follows:

* Delivery
* Quality
* Communication

--------

# Delivery

First and foremost, as a software developer, if the code you are writing isn't
going live, then you might as well not be doing it. However, this core value
goes beyond just "getting code live," as it also pertains to the manner and
standard of the work you're delivering.

## Operational Excellence

The actual go-live process must be of the highest quality. My indicators for
excellence are as follows:

* Deployments are not scary. No one should have a pit in their stomach when a
  deployment is happening.
* Deployment process is published and well-known. Anyone should be able to able
  to run a deployment.
* Deployments are repeatable
  ([idempotent](https://en.wikipedia.org/wiki/Idempotence)).
* Rollbacks are straightforward. I hate rolling a deployment back, but if it
  needs to happen, a rollback should be as simple as a deployment.
* Team at large is aware of every deployment. Communication is key here.
  Tracking application metrics post-deployment matters, as well.

## Delivered Value Matters

This one sounds weird (and its a work in progress), but the core of this value
is that getting **something** live sooner is better than getting **nothing**
live for a long time.

For example, let's assume you're working on a project for a client, and you
estimate that it won't be finished for 9 months. Nine months is a hard sell to
any client for them to see delivered value (code that went live), so getting
some percentage live sooner (40% of the project in 2 months, for example) will
bring more satisfaction to the client.

--------

# Quality

If the work you are outputting is low-quality, then it doesn't matter that it
went live. If no one can use the product you made, it's not done yet. With
respect to quality, I directly care about two main traits, detailed below.

## Code Quality

A software developer's primary output is code. Regardless of functional or
non-functional specifications, the product from a software developer is the
code. Due to this, the quality of the code must be of the highest degree. To
achieve this, I focus on two specific traits:

* Code reviews are paramount to a team's learning and improvement. Code reviews
  are open, honest, and non-personal. Everyone is invited, and anyone can
  participate.
* Developers are the primary responsible party for a delivered item's quality.
  This means that if code is rejected in the final stage of QA, the developer
  that wrote the code owns the defect.

Both of these points will have future posts dedicated to them, so stay tuned!

## Quality Assurance

While developers are inherently bad testers due to the fact that they understand
the underpinnings of the code they wrote, it stills falls on the developer to do
some semblance of quality checks.

* Developers test code they checked in. This is typically done by walking
  through the acceptance criteria locally to ensure all the points are hit.
* Developers do not punt tasks over the wall. It's never "not my job" when a
  different team needs help with a task. Developers have a seat at the table
  with every issue/feature, so they should be an active participant to work
  through any potential situations.

--------

# Communication

This is the hardest value to learn and improve upon. As developers, most of us
want to put hands-on-keyboard as much as possible, ignoring the world around us.
While this is the most efficient use of our time if we're all individual
contributors, that's rarely the case in a team or organization.

## Work Visibility

The easiest way for an organization at-large to understand what the software
development team is working on is to make it visible in an easy-to-understand
manner.

* All work done by the development team is visible to all team members. This
  allows for natural cross-talk between developers regarding the work being
  done.
* The current state of the development team is easily determined. This is
  typically done with a board in a tool like
  [JIRA](https://www.atlassian.com/software/jira) or
  [Trello](https://trello.com/).

## Honesty

There must be a social contract both within the software development team and
between the software development team and the external business teams.

* Work statuses are communicated clearly and honestly. This is typically done in
  a daily stand-up meeting (more on these later). If something isn't going
  perfectly to plan, be upfront about it to figure out the solution earlier
  rather than later.
* Reasons for work priorities are clearly communicated to all team members. If
  there's an angry client that requires Feature X to do business, **let the team
  know this**. A developer works best when she knows *why* she is working on
  something, rather than just doing it to get a paycheck.
* Development leadership communicates good, bad, and ugly to all team members.
  Things don't always go well; it is imperative that the development team is
  aware of the data that the business team is aware of. Additionally, let the
  software development team know when situations are good!
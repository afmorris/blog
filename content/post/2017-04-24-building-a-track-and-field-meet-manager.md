---
title: "Building a Track and Field Meet Manager"
date: 2017-04-24
tags: ["track and field", "software development", "csharp", "dotnet", "electron"]
aliases: ["/building-a-track-and-field-meet-manager/"]
---

Some backstory before I go into the details here: I am a track and field coach.
I run a website that displays track and field statistics throughout the state of
Ohio. I am a lead software engineer for a large ticketing company. All of these
add up to my current project: building a competitive product to [Hy-Tek’s Meet
Manager](http://www.hy-tekltd.com/track-meet-management.html).

<!--more-->

# Requirements

In order to deliver a product that can be used by as many people as necessary,
the following requirements exist:

* Must exist as both a hosted application and a client-installed application
  seamlessly
* Must be able to import Hy-Tek Meet Manager exports
* Must be able to import Hy-Tek Team Manager exports
* Must be able to interface with physical automatic timing systems
* Must be able to display live results on the web
* Must have a team and roster management component

I’m sure there will be other high-level requirements as I get to them, but this
is a good start.

# Technology

As I’m a .NET developer at heart, my initial implementation will be ASP.NET MVC
on .NET Core. This will enable me to keep my costs down, as I can run .NET Core
on any host. For the database, I will initially go with a hosted relational
database solution, most likely MySQL or PostgreSQL. The driving force here is
straightforward: the cheaper the better.

For the client-installed solution, I will be utilizing Electron on top of the
same front-end assets used in the hosted solution. I’m not 100% certain on how
to do offline data storage in the case of a no-internet situation, but I’m
certain I can pull down a SQLite database to be used locally, then write a
synchronization process to the hosted solution.

The display of live results on the web will make use of a websocket service to
push updates to connect clients. Again, in an offline situation, there will need
to be a synchronization process, so live updates will not be possible.

Right off the bat, I can see the difficult part of this project will be
interfacing with hardware components, such as the automated timing systems. This
will most likely go with a later phase, but this is an essential feature that
needs to be worked on.

# Work Tracking

I’ll be using the following public Trello board to track my progress on this
project: [https://trello.com/b/MJ9yZMP4](https://trello.com/b/MJ9yZMP4)

# Next Up

Stay tuned as I move forward in my development efforts to build out the MVP for
this product!
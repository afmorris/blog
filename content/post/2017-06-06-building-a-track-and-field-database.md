---
title: "Building a Track & Field Database"
date: 2017-06-06
tags: ["personal", "track and field", "software development"]
aliases: ["/building-a-track-field-database/"]
---

Back in the day (high school and early college), I ran one of the state's
largest track and field databases. All of the entries were entirely
crowd-sourced (before crowd-sourcing was a thing), and it was used pretty
heavily.

<!--more-->

As of late 2009, I let the updating of the website slip, and it eventually
trickled into nothing. Additionally, I did the typical developer thing of
letting my eyes be too big for my stomach. I tried to expand my database to more
sports: first cross country, and then eventually football. As I wasn't getting
paid for this gig (or the Google Adsense profit was low), I eventually stopped
working on it entirely.

Now that I'm a real bonafide developer person with his own salary, I've decided
to resurrect my pet project. My motivations for this are professional in some
ways and personal in others. Let's go over them:

* I love having a project to force me to learn a new technology. Work can't
  always provide this, so I'm doing it on my own (ASP.NET Core, here we come)
* The online track and field database landscape for Ohio is lacking. You either
  pay a fee, or you get incomplete results (or both!). My database will be
  always be free and the most up-to-date on the web.
* I love track and field. I love feeling connected to both the community and the
  sport.

So, what's the name of this project, you may ask. I'll keep it going with what
it used to be called (luckily I could still purchase the domain):
OhioTrackStats.com.

It's not up yet, and it won't be for a little while as I work through the actual
development of the project, but stay tuned! I'll try to post some technical
specs as I get through the bigger chunks of functionality.
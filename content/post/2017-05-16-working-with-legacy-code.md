---
title: "Working With Legacy Code"
date: 2017-05-16
tags: ["software development", "legacy code"]
aliases: ["/working-with-legacy-code/"]
---

Precursor: I've been working with a significantly large legacy codebase for over
three years now. Here are some tips, tricks, and general pointers to make sure
you don't make the same mistakes I've made with legacy code.

<!--more-->

# Find The Expert

The most important first step with any legacy codebase is to understand who the
Expert is. The Expert is not always the person who has written the code
previously, because that person is very likely gone. This means the Expert may
be someone you might not expect: a QA engineer, a Business Analyst, or even a
customer.

**Tip!** If you can't find the Expert, **you become the Expert**.

# Step Through The Code

Hopefully you're able to run the legacy codebase on your local environment.
Let's assume you can.

I strongly recommend you run the code locally with as relevant an environment as
possible. In my current job, we are able to run the application and point it to
our DEV/QA/PROD databases without any issues. This allows us to very succinctly
define how the application will be running with both test data and real
production data.

# Resist The Urge

Every developer, at some point or another, reads code and wants to rebuild it.
This urge is a wonderful one; we all want to be creators!

Legacy applications are an entirely different beast. Do not give in to the
creator's urge. There is so much working knowledge within the legacy codebase
that may not be clear to you the first time you read through it, and if you
attempt to rebuild it, there is a high likelihood that you will miss something.

# No Shortcuts

When you are writing new code in a legacy codebase, try to refrain from taking
shortcuts in the code. The way I think about it is "If I'm changing this now,
someone else will change it again later." Don't leave random artifacts strewn
throughout your code.

If it's worth doing, it's worth doing as correctly as possible.

# Not All Changes Are "Good"

Finally, there comes a point in legacy code development in which the
stakeholders are requesting a change that the legacy application may not easily
support in its current form.

There are two options in this situation:

1. Don't make the change
1. Make the change, but in a "bad" way

In most real-world environments, even a "bad" change is movement forward. While
it may not be perfect, typically "something" is more than "nothing." It is with
that in mind that I offer up the suggestion to be open and accepting of
less-than-desirable development decisions, especially in legacy code.
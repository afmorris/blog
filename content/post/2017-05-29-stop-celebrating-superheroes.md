---
title: "Stop Celebrating Superheroes"
date: 2017-05-29
tags: ["software management"]
aliases: ["/stop-celebrating-superheroes/"]
---

# The Death March

In most development walks of life, any given software developer will run into a
project known as a **Death March**. These projects have any number of problems,
but the most typical ones are:

* Too much scope for the timeline
* Not enough resources for the scope
* Resources have the wrong skillset for the project

<!--more-->

Management resources may wring their hands about trade-offs and how "this is a
big win for us," so all good project management principles are thrown out the
window in order to deliver this project.

In order to deliver a project with invalid scope, timeline, or resources, a
developer (or group of developers) must don his or her cape and go into
**Superhero Mode**.

The typical Superhero Mode is best characterized by longer hours (including
weekends, potentially), less external distractions (e.g., project management,
email, IM, etc.), and a general unhappiness. Some sort of compromise is set with
regard to either scope or timeline, and the Superheroes continue along their
path to completion.

## Project Completion

Assuming the company doesn't go under, most of these Superhero projects tend to
end with some reasonable amount of success, especially given the aforementioned
project issues.

Without fail, a company-wide (or location-wide) email blast is sent out,
congratulating the team for their "hard work in the face of tight deadlines" and
their "perseverance with a tough situation." A few "Good Job" emails may be sent
out, either in blast format, or directly to the Superheroes. Life moves on.
People forget in a few days.

## What Happens Next

Given an organization in which at least one Superhero Mode had to occur, there
exists a high probability of another one rolling through. However, now that the
first one has happened, there is less friction to let it occur again, because
the management resources saw great results from the first one!

`GOTO "Death March"`

Rinse. Repeat.

## The Downside

> We are operating at full efficiency! This isn't a bad thing!

I've literally heard this quote from a management resource following a
particularly bad Death March. The shortsightedness of this statement, while
galling to us development types, is merely a product of his environment. If you
only measure project success by its delivery, you are going to Miss The Point.

Instead of measuring a project's success or failure by merely its delivery, one
must begin to pay attention to the Soft Skill Measurements (employee happiness,
for one example). Some of the long-term consequences of having these Death
Marches are listed below:

* Employee Dissatisfaction
* Lack of Long-Term Career Growth
* Constant Context-Shifting Leads to Nothing Getting Done
* Employees Leaving the Organization

# How Can We Fix This

Without trying to live in a fantasy world in which these types of projects don't
exist (hint: they will always exist), we must, instead, try to correct what we
have control over.

## Start the Project

When a Death March project is coming down the pipe, we must realize this early.
Some quick symptoms of a Death March are:

* Unmovable timeline
* Development team balked at scope
* Ambiguous scope

If any of these three items are true for an upcoming project, especially the set
timeline, the project **must begin** as soon as possible, even if it's someone
ambiguous. Giving the team as much time as possible is the best way to ensure
success on a project.

## Stop Celebrating

While I don't have a problem with a management resource thanking me or my team
for a job well done, especially in the face of a Death March, the email blasts
that congratulate and recognize a team for delivering a Death March can be
detrimental.

> Stop saying "Thank you." Start saying "I'm sorry."

Instead of hearing "Thank you" and "Good job" continuously in the post-project
phase, I recommend that leaders begin to apologize for the situation (show some
humanity!), and then follow it up with an explanation of **why** the Death March
occurred and **what** the organization is doing to ensure it doesn't happen
again.

Having a human explanation for the reasoning behind a Death March and a plan
moving forward for what the organization is doing to protect its employees from
future Death Marches is a sure sign of a mature organization. If the
organization you work for cannot (or will not) give its employees those two
items, then **they do not care about Death Marches.**

# Wrapping It Up

To put it succinctly: Death Marches happen, and there's little a development
team can do about them. I challenge development leaders to put this reality in
perspective and begin to work with organization leaders to provide more human
communication and expectations.
---
title: "My NFL Fealty"
date: 2015-12-26
tags: ["personal", "sports"]
aliases: ["/my-nfl-fealty/"]
---

As far as the NFL goes, I am a lifelong Cleveland Browns fan. Yes, I realize
this means I love self-loathing and hate feeling happy on Sunday evenings.

<!--more-->

However, I identify much more strongly to the college that I graduated from: The
Ohio State University.

Let's compare and contrast the two teams.

# The Ohio State University

Win-Loss Record in the last 10 years: 111–20 (0.847) Last National Championship:
2014 Number of sub-.500 seasons in the past 10 years: 1

# Cleveland Browns

Win-Loss Record in the last 10 years: 51–102 (0.333) Last NFL Championship: 1964
Number of sub-.500 seasons in the past 10 years: 9

I am a firm believer that for anything to be worth my time and/or effort, it
should have some **return on investment**. This means, in the case of a football
team, they need to **fulfill the promise**. Which promise, you ask? The promise
of any organized sport team: to compete at a high level for the ultimate goal of
winning a championship.

Simply put, the Browns just don't do that. I've determined that my fandom for
the Browns must be put to an end, and I've gone hunting for a team to be "my
team." And I've come to a decision: it's going to be the **Carolina Panthers**.

--------

# My Reasoning

How did I come to the decision that a team in which I have no real connection to
should be "my team?" Let me count the ways.

1. Carolina currently has 4 past Ohio State football players: WR Ted Ginn Jr.,
   WR Philly Brown, S Kurt Coleman, and OG Andrew Norwell.
2. Carolina is dedicated to winning, as evidenced by their recent 4 year
   improvement.
3. Cam Newton.

That's right, I owe sports fealty to Cameron Jerrell Newton, specifically.
Follow along, kids, as we go back in time through Cam Newton's journey to the
NFL.

Cam Newton was originally recruited by Urban Meyer at the University of Florida
as the heir-apparent to Tim Tebow. In fact, he actually saw some success on the
field during his time at Florida, but ultimately his time was short-lived.

On November 21, 2008, Cam stole some laptops from a dorm room, and the eventual
fallout led him to transfer from the University of Florida. He then hopped
around to a Junior College before going to Auburn and winning a National
Championship and the Heisman Trophy.

"How does this affect you, Tony?" you might ask. Well, if anyone would pay
attention to the above statements, you would realize that, above all else, I am
a gigantic Ohio State fan. Ohio State's current coach is Urban Meyer. That's
right, folks — the same Urban Meyer that originally recruited Cam Newton.

Let's say Cam Newton never steals those laptops. He then never transfers to a
JuCo. He eventually follows in Tim Tebow's footsteps as the starting quarterback
for the University of Florida. He wins there as well as he did at Auburn.
Florida never has a couple of "down" years during Urban Meyer's tenure. Urban
never leaves the University of Florida. Urban never comes to Ohio State. Ohio
State is mired in a number of losing records with Luke Fickell as its head
coach. I am measurably sadder than I am today.

That's it, folks. I owe NFL Fealty to Cam Newton and the Carolina Panthers
because he single-handedly brought me football happiness in the form of Urban
Meyer.

--------

*Note: Obviously I am not dropping the Browns as my team. This would cause my
grandfather to disown me. Instead, I think I will just pick up Carolina as my
favorite NFC team, and keep being 50% sad on Sundays.*
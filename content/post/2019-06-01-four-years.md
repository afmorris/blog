---
title: "Four Years"
date: 2019-06-01T22:16:33-04:00
tags: ["family", "personal"]
---

Four full trips around the sun have passed since my mother, Kathy, passed away after an extended battle with lung cancer. Since 2017, I began posting what she's missed in the past year as a sort of retro-diary to her.

<!--more-->

# Previous Years

* [Three Years]({{< ref "2018-06-02-three-years.md">}})
* [Two Years]({{< ref "2017-06-01-two-years.md">}})
* [One Year]({{< ref "2016-06-01-one-year.md">}})

# June 2018

* Tommie runs at state again to finish her Junior year. She didn't run her best race, but she learned a lot, and she'll be back with a vengeance.
* The Morris/Kurtz clan goes to Disney! Cindy, the kids, Carol, and I all had a great time. It was exhausting, but boy oh boy was it great. We'll be back... sometime.

# July 2018

* Cindy and I go to an Indians game, courtesy of my employer. We got box seats behind home plate, and I finally got a foul ball. I've legitimately been to dozens of baseball games in my life, and this was the first time it happened. I was over the moon about this. Plus, it was great to watch a game with my best friend in a wonderfully comfortable setting.
* Mattie's first Indians game! Her and I went to the game with some friends. She didn't really understand the baseball part, but my goodness was she excited about the giant hot dogs.
* Cindy, Carol, the girls, and myself all visit the Cleveland Zoo for the Asian Lantern Festival. We're members of the zoo, so we go semi-regularly, but this evening event was amazing! Plus, Mattie and Macie got to watch the elephants, and let's just say they're mesmerized.

# August 2018

* You turned (checks the calendar...) 55 this year! I miss you all throughout the year, but your birthday is particularly difficult for me, as are other family events.
* David's getting up there in age, too. The big 3-4 this year!
* Tommie started her **senior year** of high school, and Joey is a **junior**. Where did all this time go? You would have given me some words of wisdom on how to make it feel slower, I'm sure.
* I switched jobs (again, I know). I'm now at Hyland in Westlake, working as a Principal Architect within their Cloud program. Lots of words that basically say "I'm really focusing on improving the delivery of the software we create." You wouldn't have understood this, but you would have been so proud of me.
* We announced to the world (social media) that Cindy and I are expecting Baby Morris #3 in late March! I am generally concerned that I'm going to be sharing a birthday with this little one. Also, I had a serious amount of anxiety that we were going to have multiple babies this time around. While that would've been _exciting_, I am pretty okay with it not being the case.

# September 2018

* Cindy and I have been married for **four years** already! Together for over seven years in total.
* Another world announcement: Baby Morris #3 is a boy! **I am getting a son**! I wish you were around for this so I could pick your brain about how to raise a son.

# October 2018

* Macie turns **ONE**. When did this year pass by? She's such an amazing little girl, and she just keeps growing and adapting every day. You'd be in love with this little one. She's so similar, and yet so much different, from Mattie.
* Cross country meets galore. I know David and I never competed in cross country, but I think you would have enjoyed coming to the meets.
* Speaking of cross country, Tommie's final cross country meet was in October. It's crazy to think that it's just all over, just like that. It didn't end how she wanted it to, but I am so proud of the runner and person she has become throughout her ups and downs the past four years. You'd love this girl, Mom.
* I attended a technical conference in Las Vegas. It was my first time in Vegas, and oh man was this place **HUGE**. Great time, in general, and I can't wait for more opportunities like this.
* Halloween! Mattie (along with Tommie and her friend Sara) were all Disney princesses. Mattie was an adorable little Belle. Macie was a cute little cactus. It was cold and rainy, but they all had a great time.

# November 2018

* Tommie turns 18! We celebrated with a little party at a place called PLAY:CLE, which has things like rock walls and obstacle courses. I climbed a rock wall for the first time in my life, and (separately) I got stuck on one of the ropes courses. It was hilarious for everyone not named Tony.
* Ohio State beat Michigan 62-39. We had our typical party, and it was a great time with both friends and family.
* Family tree decoration time! Our yearly ritual with our growing family is one of my favorite times of the year.

# December 2018

* My little girl, Mattie, turns 3 years old. We celebrated with a small family get-together, which was just great. Mattie adores Grandma, Grandpa, Kevin, David, and Lindsey, even though they don't get to see her all the time. It's so heartwarming to see her grow up and interact with our little family.
* We visited a little street in Strongsville that has some of the most ridiculous Christmas decorations I've ever seen. They essentially shut down an entire neighborhood because so many people come to visit. The whole family came along, and we had a great night out together.
* Next up on the Christmas tour: Santa Claus meet-and-greet with the whole family. Mattie and Macie definitely don't trust the fat man in the red suit, so we get some really great pictures. You would've loved to hear the stories that come out of these situations.
* Christmas! We went down to Dover again for our little family Christmas. As in previous years, this one day is the hardest day of the year for me, but it's great to see all the love.
* Austin and his wife Terri had their son, James! Not directly related to our specific family, but Austin's essentially a brother, so you would have loved to know this.

# January 2019

* We hosted Cindy's family Christmas. Her sister's family, her brother's family, and us all jammed into our house. Gifts galore and happy kids everywhere made for an absolutely great time.
* Joey turns 17! He's getting way too old, and I, for one, do not like it. He's honestly everything I could ask for a big brother to the little ones.
* We celebrated Joey's birthday by going to SkyMax, a trampoline arena. A few hours of jumping around really gives you a workout, but it was generally a great time had by everyone.
* We tested the girls' love of the snow. Guess what? They **absolutely love it** (until it gets in their snow suits). Honestly, their snow suits are some of the cutest things on the planet.
* Disney On Ice for the second year in a row! We splurged and got the VIP tickets this year, which let Mattie and Macie have a meet-and-greet with Belle (who read them a story) and Mickey! Mattie loved every minute of it, and they both had an absolute blast during the Disney On Ice show itself!

# February 2019

* Mattie is such a goof. She "does experiments" in the bathroom sink all the time. And by "experiments," I mean that she pours water from one beaker to another for hours on end.
* First indoor track meet of the year! I'm coaching shot put again this year for Cloverleaf, and I absolutely love every minute of it. I'm sure you would have made it to any number of meets throughout the year.
* Tommie is getting recruited by colleges to run. We figured this was going to happen, but it is still one of those things that is really interesting if you've never gone through it before. Super exciting though!

# March 2019

* Micah Frederick Morris was born at 2:32am on March 21st. He's super healthy, super long, and just amazing. Also, I'm kind of excited that we don't share a birthday. He's going to need his own day for all the love he's going to get.
* The big 3-3 for me. Every year since around 25 has been better than the last (hint: Cindy and I started dating that year), so I'm certain this year is going to continue that trend.
* Tommie, Mattie, Macie, and I visited the Cleveland Children's Museum before heading to PAW Patrol Live. The girls loved every minute of it, and the show was even better. I had such a great time with all my girls. Tommie's the best big sisters I could ask for the girls.
* Chris and his wife Lauren had their son, Graham! So many boys in our group now!

# April 2019

* Track meets galore. I love this part of the year. I miss you at every sporting event, as that's one of my prominent memories about you. You were always there for every game, match, and meet.
* Easter! We decorated eggs for the first time with the little ones! You would have loved being part of this new tradition we're putting together with the babies. We then spent Easter Sunday at Carol's house with the extended family, and we had just an absolutely great time with everyone. Micah was a hit, and the rest of the kids are just such good kids.
* Senior Night for Tommie. Her last home meet for Cloverleaf ever. Very bittersweet with this. It was a great evening, and she did great. It was wonderful to be able to send off all the seniors with a win.

# May 2019

* Senior Prom for Tommie. She looked absolutely gorgeous. I don't know when she changed from "girl" to a "young woman," but it's nights like these that make me realize that it most definitely happened.
* More Tommie news: she committed to run both cross country and track at the University of Mount Union! So proud of this girl, and I can't wait to see what she does collegiately. In addition to her major college decision, she qualified for the state meet in the 800 again!
* Bryan gets married! Brittany is wonderful, and you would've loved to be able to make it to the ceremony. Micah and James got to hang out for the first time!

# June 2019 (so far)

* Tommie finishes her high school career with a **sixth-place** finish at the state meet in the 800! She ran so unbelievably well, and you would've been just so proud of her. She had a pretty serious cheering section, as David, Lindsey, Kevin, and Grandpa all made the trek to Columbus to watch her run.
* Tommie graduated high school. It was a great ceremony, and I can't wait to see what she does in college now!
* The children (all of them!) are all growing up to be happy, healthy, mostly well-adjusted kids. Cindy is a superhero, and I am doing my absolute best based on what you taught me growing up.

--------

I miss you so, so much. This feeling of grief never goes away, but I learn to live with it a little more each day. You're constantly in my thoughts, and I see you in everything I do. I see you in my children. Every day I try to make sure I'm making the memory of you proud. Until next year. Love you.
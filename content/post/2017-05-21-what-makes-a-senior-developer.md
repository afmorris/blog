---
title: "What Makes a Senior Developer"
date: 2017-05-16
tags: ["software development", "career advice"]
aliases: ["/what-makes-a-senior-developer/"]
---

In my current job, I'm frequently asked to interview new candidates to join our
development organization. One of the first questions that always gets asked,
either by me or one of my colleagues, is "What level is this person interviewing
for?" There really are only two answers for this question where I work:
Developer or Senior Developer.

<!--more-->

Let me drop a disclaimer here real quick: the development organization in which
I work is **really really bad** at defining explicit expectations on a per-level
basis. Because of this, my colleagues and I are forced to develop implicit
expectations for the two levels. This post is an attempt to formalize the way I
see the difference between the roles.

# Writing Code

I fully expect both the Developer and Senior Developer level to be able to solve
technical problems by writing code. Additionally, I expect each of the two roles
to be able to take a defined story, implement it, validate it locally (Or in a
development environment), and deliver it to the QA team.

The gap that I typically see between the two roles is that the Senior Developer
should be able to take a poorly-defined story, vet the requirements with the
product owner/project manager/stakeholder, and still deliver a high-quality
implementation. The Senior Developer can be tasked with dealing with the
inevitable back-and-forth that comes with a poorly-defined story and still come
out ahead.

# Technical Delivery

I frequently say that "Code is useless until it's live." This is true in all
industries and at all organizations.

> Code is useless until it's live.

Because of this, the delivery of a developer's code is, oftentimes, more
important than the actual development itself. A good Developer will write solid,
tested code and deliver it to the QA team with minimal issues for the defined
stories. A good Senior Developer will do all of that, and she will also ensure
quality delivery to the production environment, as well.

This attention to detail to the production environment is essential in an
organization with 30+ developers, all trying to get his code live in some way.
As I'm typically tasked with ensuring our production deployments succeed, I can
immediately tell the difference between an implementation prepared by a
Developer vs. a Senior Developer based on the amount of questions I have about
it.

# Communication

The most important difference between a Developer and a Senior Developer centers
around a completely non-technical skill: communication.

Being able to communicate the technical reasons and drawbacks of an
implementation is one of the most important skills a "junior" developer can
possess. However, as one progresses through her career, it becomes essential
that she becomes adept at discussing the changes in a non-technical way. This
may include a conversation about functional requirements with the product
owner/business analyst, timeline impact with the project manager, user
experience changes with the UX/UI designers, or any other of a myriad of
possibilities.

The easiest way I can denote between a Developer and a Senior Developer is the
ease at which a non-technical colleague (e.g., product owner, project manager,
etc.) can get an update on the developer's implementation and how it's
progressing. If the developer can only speak in technical lingo and is having
trouble changing his dialect to match the audience, then he is, typically,
merely a Developer.

# Wrapping It Up

The items listed above are just three in a number of differences between a
Developer and a Senior Developer. These items are the ones that I personally key
on with respect to both internal feedback processes and interviews.

What do you think makes a Senior Developer?
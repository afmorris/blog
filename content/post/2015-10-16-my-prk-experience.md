---
title: "My PRK Experience"
date: 2015-10-16
tags: ["personal", "health", "retro-diary"]
aliases: ["/my-prk-experience/"]
---

I was born with absolutely atrocious eyesight. I always say that if I were born
in the wild, there’s absolutely no way I would have survived.

<!--more-->

> If I were born in the wild, there’s absolutely no way I would have survived.

I got glasses when I was 9, and I went to contact lenses at 14. That was 15
years ago. After today, I hope to never have to wear contacts ever again.

I arrived at Laser Vision Centers of Cleveland at 8:15am with my wife. After
finalizing my payment (thanks for the 20% discount, VSP!), I went through a
couple final tests with the technician to make sure my eye topography hadn’t
changed in a week. All good there!

From there, the optometrist on staff took me back to do a final check on my
prescription strength. I’m glad they were thorough with ensuring everything
would come out correctly. Prescription all set, now time to meet the surgeon.

After popping the first of my Valium, I talked with the surgeon about the
procedure. He went into detail on what I would be going through. Nothing
surprising, as I had already done some serious research into the procedure.

With the pre-op meetings all done, it was time to prep for the surgery. I
channeled my inner lunch lady and wore a sweet hair net. First up was a round of
numbing eye drops, which worked almost instantly. Next up was a round of
antibiotic eye drops, followed by a sponge of lidocaine on the inside of both
eye lids. This was to help the eye speculum from irritating my eye lids. After
having my eye lids and eye lashes wiped with iodine, I was ready off to the
surgery room.

As I was doing PRK and not LASIK, I only had one laser machine to sit under. I
laid under the machine on the surgical bed.

My right eye was first. The eye speculum held my eye lids open. The surgeon
wiped my eye with an alcohol solution to loosen the epithelial layer, then wiped
off the loosed cells. I was told to watch the green light. 18 seconds later, a
slight smell of burning hair, and it was over. A quick antibiotic wash followed
by seemingly 400 gallons of cold water wash, and I could see! The doctor put a
non-prescription contact lens on my eye as a bandage of sorts, and my right eye
was done.

> 18 seconds, a slight smell of burning hair, and it was over.

The procedure was repeated on my left eye. It took 19 seconds for my left eye
due to differences in prescription.

I can’t explain how much this changes everything. I could see immediately. Not
just shapes and colors, but I could really SEE.

> I could see immediately.

Now, with PRK, I am not able to see 20/20 immediately. In fact, I am seeing a
little blurry as I type this. It will actually get more hazy before it begins to
clear. This is due to the epithelial layer of cells that was scraped off during
the process coming back to heal. In about 5 days, my eyes will be about done
healing.

Words cannot accurately describe how impressed I am by both the technology of
modern medicine and its convenience. This is a life changing procedure, and I
was done by 9:30am. 1.5 hours for sight. Absolutely unbelievable.
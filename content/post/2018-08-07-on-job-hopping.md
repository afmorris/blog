---
title: "On Job Hopping"
date: 2018-08-07
tags: ["career advice", "personal"]
aliases: ["/on-job-hopping/"]
---

Since graduating from Ohio State in March 2009, I have started at six different
companies. I either stick around for a long time, or I'm out within 4-8 months.

<!--more-->

1. Rosetta (04/09 - 09/13: 4 Years, 5 Months)
1. OverDrive (09/13 - 05/14: 7 Months)
1. Veritix/AXS (05/14 - 08/17: 3 Years, 4 Months)
1. Pleasant Valley (08/17 - 03/18: 6 Months)
1. Blue Chip (03/18 - 08/18: 5 Months)
1. Hyland (08/18 - Current)

It is conventional wisdom that changing jobs quickly (less than a year,
generally) is considered a **bad** thing, and it will ensure that I will have
less job opportunities in the future.

If all that is true, how do I keep getting hired? Is it possibly because times
have changed, especially within the technology space? Is it because I'm a
millenial, and the rules just don't apply to us?

# 😬 Probably Not 😬

I concede that it's not the best idea in the world to have a bunch of short
stops at different organizations. Any sort of pattern that moves the needle away
from "great person to hire" and toward "he might leave at any moment" isn't
exactly the best situation to be associated with. However, circumstances
sometimes take us down the path of shorter stops at organizations. Below are my
two main tips on making sure I don't get bitten in the ass when I get the
inevitable question of "It seems like you've been hopping a bit lately?"

## Have a Cohesive Story

Every career change I've made has been for a very specific set of reasons,
listed by order of importance:

1. Good for my family
2. Opportunity to work with close friends
3. Good for my career

The move from Rosetta to OverDrive was due the growth opportunity that OverDrive
was offering. The move from OverDrive to Veritix was to work with my close
friends. The move from AXS to Pleasant Valley was good for my family as it was
15 minutes away and offered me more time at home with my growing family. The
move from Pleasant Valley to Blue Chip was entirely about family opportunities,
allowing me to coach my children at their high school. And finally, the move
from Blue Chip to Hyland affords me the opportunity to work with another of my
close friends.

As you can see, I am not talking about "running away" from any of the
organizations that I've left. Of course there have been negative aspects of
every place I've worked--I'm not that much of an idealist. Focusing on the
negative aspects of leaving an organization, however, can put a bad taste in any
potential opportunity's mouth.

> Focusing on the negative aspects of leaving an organization, however, can put
> a bad taste in any potential opportunity's mouth.

By driving home the "this is the best opportunity for myself, my career, and my
family" narrative, it ensures that you don't focus on bad aspects of splitting
up from an organization.

## Never Burn Bridges

Leaving an employer is tough. A former boss of mine described the recruiting and
hiring process as getting married; it stands to reason that the leaving process
is like getting divorced. There can be finger pointing, hurt feelings, and
lasting negative impressions.

The easiest way to combat these types of bad situations is to be an exemplary
employee for as long as possible. My typical resignation story is as follows:

1. Hand deliver the resignation letter to your direct manager. If he/she is not
   available, then give it to your HR generalist or representative. This is
   particularly important to do as soon as possible in order to give the
   organization as much time as possible to handle your leaving.
1. When discussing your resignation with your manager, be sure to have a
   conversation regarding when to tell people. Some organizations like to hold
   certain information close to the vest, while others don't care as much.
1. Inform your peers and project teams of your upcoming split (if applicable).
   The earlier they know, the better life will go for the next two weeks.
1. Prepare any and all documentation about items that you own. This can be
   technical projects, client contacts, upcoming dates, project statuses, or
   anything in between. The more documentation you have ready to go, the better
   the transition is going to be.
1. **Bust. Your. Ass.** You never want to be the person that slacked off the
   last two weeks of employment, leaving a shitstorm behind. Be the person the
   organization tells stories about after you leave. Be the example for future
   employees.
1. Say good-bye to everyone in a courteous, professional manner. Remember: this
   is a divorce, and it is hard. Some people may not react well. Wish them well
   in the future, and tell them to reach out to you for anything they might
   need.

I can't emphasize #5 above more. I live in the Cleveland area. It may be
generally large from a population point of view, but the technology sector is
still small and growing. People talk to other people, and you never want to have
a bad experience with one resignation bleed into another opportunity.

> Be the person the organization tells stories about after you leave.
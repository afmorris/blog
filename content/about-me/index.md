---
title: About Me
---

Hello, I'm Tony Morris. I live in Seville, OH with my wife, five kids, and two dogs.

I am currently an Enterprise Architect at [Hyland](https://www.hyland.com/) focusing mainly on improving the delivery pipeline across the organization, especially with respect to public cloud providers.

I consider myself a reasonably experienced web developer, specifically in the .NET stack of technologies. The reason that I'm "reasonably experienced" is because I know the best place to find most of the answers I come across: the Internet.

If you wish to chitchat with me, I am pretty active on [Twitter](https://twitter.com/tonytalkstech).